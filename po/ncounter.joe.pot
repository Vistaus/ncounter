# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ncounter.joe package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ncounter.joe\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-06 00:34+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/ResetDialog.qml:33
msgid "Clear ALL settings? Cannot be undone."
msgstr ""

#: ../qml/ResetDialog.qml:43 ../qml/Settings.qml:155
msgid "Reset"
msgstr ""

#: ../qml/ResetDialog.qml:61
msgid "Cancel"
msgstr ""

#: ../qml/AboutApp.qml:31 ../qml/Main.qml:34 ../qml/Home.qml:43
#: ../qml/Settings.qml:46 ncounter.desktop.in.h:1
msgid "nCounter"
msgstr ""

#: ../qml/AboutApp.qml:47
msgid "About nCounter"
msgstr ""

#: ../qml/AboutApp.qml:61
msgid "Version: "
msgstr ""

#: ../qml/AboutApp.qml:65
msgid ""
"How long has it been since the last nCounter?\n"
"Or how long until the next?\n"
"Enter the event and date to find out!"
msgstr ""

#: ../qml/AboutApp.qml:71
msgid "Report Bugs"
msgstr ""

#: ../qml/UpdateDialog.qml:33
msgid "%1 updated"
msgstr ""

#: ../qml/UpdateDialog.qml:41 ../qml/TrackingDialog.qml:41
#: ../qml/EventDialog.qml:41
msgid "OK"
msgstr ""

#: ../qml/Home.qml:67
msgid "No nCounter event"
msgstr ""

#: ../qml/Home.qml:68 ../qml/Settings.qml:111
msgid "Check nCounter"
msgstr ""

#: ../qml/Home.qml:86
msgid "Add event to start counting"
msgstr ""

#: ../qml/Home.qml:86 ../qml/Home.qml:91 ../qml/Home.qml:93
#: ../qml/Settings.qml:139 ../qml/Settings.qml:148
msgid ""
":\n"
"Today"
msgstr ""

#: ../qml/Home.qml:86 ../qml/Home.qml:91 ../qml/Settings.qml:139
#: ../qml/Settings.qml:148
msgid ""
":\n"
"%1 until"
msgstr ""

#: ../qml/Home.qml:86 ../qml/Home.qml:91 ../qml/Settings.qml:139
#: ../qml/Settings.qml:148
msgid ""
":\n"
"%1 ago"
msgstr ""

#: ../qml/Home.qml:105
msgid "+"
msgstr ""

#: ../qml/Home.qml:144
msgid ""
"And not only so, but we also boast in our tribulations, knowing that "
"tribulation produces endurance"
msgstr ""

#: ../qml/Home.qml:145
msgid "Knowing that the proving of your faith works out endurance"
msgstr ""

#: ../qml/Home.qml:146
msgid "Rejoice in hope; endure in tribulation; persevere in prayer"
msgstr ""

#: ../qml/Home.qml:147
msgid "The discretion of a man makes him slow to anger"
msgstr ""

#: ../qml/Home.qml:148
msgid "Better is the end of a thing than its beginning"
msgstr ""

#: ../qml/Home.qml:149
msgid "Better is patience of spirit than haughtiness of spirit"
msgstr ""

#: ../qml/Home.qml:150
msgid "Love suffers long. Love is kind; it is not jealous"
msgstr ""

#: ../qml/Home.qml:151
msgid ""
"With all lowliness and meekness, with long-suffering, bearing one another in "
"love"
msgstr ""

#: ../qml/Home.qml:152
msgid "And endurance, approvedness; and approvedness, hope"
msgstr ""

#: ../qml/Home.qml:153
msgid ""
"And let endurance have its perfect work that you may be perfect and entire, "
"lacking in nothing"
msgstr ""

#: ../qml/Home.qml:154
msgid ""
"And in knowledge, self-control; and in self-control, endurance; and in "
"endurance, godliness"
msgstr ""

#: ../qml/Home.qml:155
msgid "Do. Or do not. There is no try."
msgstr ""

#: ../qml/TrackingDialog.qml:33
msgid ""
"Now tracking:\n"
"%1"
msgstr ""

#: ../qml/Settings.qml:68
msgid "Event Name & Date:"
msgstr ""

#: ../qml/Settings.qml:74
msgid "Input event name"
msgstr ""

#: ../qml/Settings.qml:84
msgid "Set Today"
msgstr ""

#: ../qml/Settings.qml:130
msgid "Track"
msgstr ""

#: ../qml/Settings.qml:130
msgid "Update"
msgstr ""

#: ../qml/Settings.qml:148
msgid "Add event and date, then press 'Track'"
msgstr ""

#: ../qml/modules/DaysUntil.qml:31 ../qml/modules/CalcDays.qml:31
msgid "Year(s)"
msgstr ""

#: ../qml/modules/DaysUntil.qml:32 ../qml/modules/CalcDays.qml:32
msgid "Month(s)"
msgstr ""

#: ../qml/modules/DaysUntil.qml:33 ../qml/modules/CalcDays.qml:33
msgid "Day(s)"
msgstr ""

#: ../qml/modules/DaysUntil.qml:34 ../qml/modules/CalcDays.qml:34
msgid "and"
msgstr ""

#: ../qml/modules/DaysUntil.qml:35 ../qml/modules/CalcDays.qml:35
msgid "A moment"
msgstr ""

#: ../qml/modules/DefaultHeader.qml:36
msgid "About App"
msgstr ""

#. TRANSLATORS: Description of the menu item
#: ../qml/modules/DefaultHeader.qml:43
msgid "Settings"
msgstr ""

#: ../qml/EventDialog.qml:33
msgid "Please enter an event."
msgstr ""
